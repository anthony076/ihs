
let { checkParaExist, checkParamName, execCommand } = require("./utils/functions")

const Koa = require("koa");
app = new Koa();

const connection = { ip: "127.0.0.1", port: 5566 };

// =======================
// Work Flow
// =======================
app.use(async (ctx, next) => {
  await next()
});

// middleware = [checkParaExist, checkParamName, execCommand]

// 檢查是否提供參數
app.use(checkParaExist)

// 檢查參數名稱是否正確的中間件
app.use(checkParamName)

// 執行命令的中間件
app.use(execCommand)

app.listen(connection.port, connection.ip, null, () => {
  console.log(`Listening on http://${connection.ip}:${connection.port}`);
});
