const fetch = require("node-fetch");
const testCase = require("./cases")

describe("test fetch", () => {

  const testFn = async (input, expected) => {
    const response = await fetch(input);
    const jdata = await response.json();
    expect(jdata).toEqual(expected);
  };

  for (var condition of testCase) {
    it(condition.name, testFn.bind(this, condition.input, condition.expected));
  }
});
