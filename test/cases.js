module.exports = [
    {
        name: "not provide params",
        input: "http://127.0.0.1:5566",
        expected: { status: "error", msg: "please provide query params" }
    },
    {
        name: "wrong param name",
        input: "http://127.0.0.1:5566?ab=123&ba=456",
        expected: {
            status: "error",
            msg:
                "parameter name error, use http://127.0.0.1:5566?url=xxx&out_dir=xxx&out_filename=xxx"
        }
    },
    {
        name: "correct usage",
        input:
            "http://127.0.0.1:5566?url=https://cdn.acloudvideos.com/video_ads/nutaku/armor-blitz.mp4&out_dir=C:/Users/ching/Desktop/aa&out_filename=aa.mp4",
        expected: {
            status: "ok",
            msg: "success add to idm ...",
            command:
                "IDMan.exe -d https://cdn.acloudvideos.com/video_ads/nutaku/armor-blitz.mp4 -p C:/Users/ching/Desktop/aa -f aa.mp4 -q"
        }
    }
];