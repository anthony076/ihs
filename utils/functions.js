let config = require("./config")
var { exec } = require("child_process");

async function checkParaExist(ctx, next) {
    let qs = ctx.request.query

    // 有提供 query-string
    if (Object.keys(qs).length > 0) {
        // 跳到下一個中間件，檢查參數名稱是否正確
        await next()

    } else {
        // 沒有提供 query-string
        ctx.body = JSON.stringify({
            status: "error",
            msg: "please provide query params"
        });
    }
}

async function checkParamName(ctx, next) {
    let qs = ctx.request.query
    let url = qs["url"] || undefined
    let out_dir = qs["out_dir"] || undefined
    let out_filename = qs["out_filename"] || undefined

    if (url == undefined || out_dir == undefined || out_filename == undefined) {
        ctx.body = JSON.stringify({
            status: "error",
            msg: "parameter name error, use http://127.0.0.1:5566?url=xxx&out_dir=xxx&out_filename=xxx"
        })

    } else {
        await next()
    }
}

function genCommand(toolname, setting) {
    let command = config[toolname].exe
    let abbr = config[toolname].opts

    for (key in setting) {
        command += " " + abbr[key] + " " + setting[key]
    }

    for (item of abbr.others) {
        command += " " + item
    }

    return command

}

async function execCommand(ctx) {
    let qs = ctx.request.query
    let result = ""

    let info = {
        url: qs.url,
        out_dir: qs.out_dir,
        out_filename: qs.out_filename,
    }

    let command = genCommand("idm", info)

    try {
        exec(command, (err, stdout, stderr) => {
            console.log(stdout)
        })

        result = "success add to idm ..."

    } catch (e) {
        console.log(e.toString())

        result = "fail add to download ..."
    }

    ctx.body = JSON.stringify({
        status: "ok",
        msg: result,
        command: command
    });
}

module.exports = {
    checkParaExist, checkParamName, genCommand, execCommand
}