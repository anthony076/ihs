
module.exports = {
    idm: {
        exe: "IDMan.exe",
        opts: {
            url: "-d",
            out_dir: "-p",
            out_filename: "-f",
            others: ["-q"]
        }
    }
}