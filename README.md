# IDM-Handle-Server, ihs-server

### This project is a helpful tool for IDM. The server could receive download information via http request, and trigger idm to start the download with user-defined path and filename. Use the tool, you could download file without check information on IDM GUI.

# Install 
- install ihs-server<br>
  `npm install`
- install ihs-extension<br>
    - open dev mode, and click on `Load from unpackaged` button<br>
    ![](doc/install-1.png)
    - point to ihs_extension folder<br>
    ![](doc/install-2.png)

# Usage
- start ihs server by execute `rub.bat`
- click on `ihs-extension icon`, then input url and filename
- click on `Send` button

# Demo
![](doc/demo.png)