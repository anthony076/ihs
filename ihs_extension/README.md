# ihs extension 

## Install
- open dev mode, and click on `Load from unpackaged` button<br>
![](doc/install-1.png)

- point to ihs_extension folder<br>
![](doc/install-2.png)

## Usage
- start ihs server
- click on `ihs-extension icon`, then input url and filename
- click on `Send` button

## Demo
![](doc/demo.png)